﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

/* Clase que almacena el nivel de dificultad que ha seleccionado el jugador en el menú principal */

public class DifficultyAgent : MonoBehaviour {

    // Elemento UI Dropdown del menú con las opciones de dificultad
    [SerializeField]
    public Dropdown dropDown;

    /// <summary>
    /// Dificultades con el número de respuestas sobre las que va a tener que elegir la IA (0 = todas las existentes)
    /// </summary>
    public enum Difficulties: int {Easy=0 , Normal=8 , Hard=4 , Insane=2}
    /// <summary>
    /// Valor actual del dropDown
    /// </summary>
    private static int dropDownCurrentValue;
    
    // La inicialización de la dificultad será la del valor por defecto del dropdown
    void Start() {
        SetDifficulty();
    }

    /// <summary>
    /// Establece al atributo estático dropDownCurrentValue el valor del elemento UI dropDown
    /// </summary>
    public void SetDifficulty() {
        dropDownCurrentValue = dropDown.value;
    }

    /// <summary>
    /// Devuelve un elemento dificultad del enum dependiendo del último valor asignado a dropDownCurrentValue
    /// </summary>
    /// <returns>Dificultad</returns>
    public static Difficulties GetDifficulty() {
        if(dropDownCurrentValue == 0) {
            return Difficulties.Easy;
        } else if (dropDownCurrentValue == 1) {
            return Difficulties.Normal;
        } else if (dropDownCurrentValue == 2) {
            return Difficulties.Hard;
        } else if (dropDownCurrentValue == 3) {
            return Difficulties.Insane;
        } else {
            // Si existe un error en el valor, devuelve la dificultado 'Normal'
            Debug.LogError("Not expected DropDown value");
            return Difficulties.Normal;
        }
    }
}
