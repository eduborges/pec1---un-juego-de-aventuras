﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Clase que controla las transiciones (por medio de los parámetros) del Animator del GameObject Coin */

public class CoinAnimationController : MonoBehaviour {

    // Componente animator del GameObject Coin
    [SerializeField]
    public Animator coinAnimator;

    /// <summary>
    /// Animator de la moneda
    /// </summary>
    private static Animator _myAnimator;
	
    public static Animator myAnimator {
        get { return _myAnimator; }
    }

	void Start () {
        _myAnimator = coinAnimator;
	}

    /// <summary>
    /// Provoca la animación de lanzar la moneda
    /// </summary>
    /// <param name="done">Callback booleano</param>
    /// <returns>IEnumerator</returns>
    public static IEnumerator flipIt(System.Action<bool> done) {

        // Inicia la animación de lanzar la moneda
        myAnimator.SetBool("flip", true);

        yield return new WaitForSeconds(3f);

        done(true);
    }
}
