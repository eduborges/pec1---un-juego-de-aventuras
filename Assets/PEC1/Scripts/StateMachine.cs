﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine;

/* Clase que controla todo el flujo de la partida a través de una máquina de estados */

public class StateMachine : MonoBehaviour {

    /* Clase que representa cada nodo de la máquina de estados */
    public class State {
        /// <summary>
        /// Lista de estados siguientes al actual
        /// </summary>
        public State[] nextState;
        public delegate void StateVisited();
        /// <summary>
        /// Función delegada que define que ejecutar en cada estado una vez visitado
        /// </summary>
        public StateVisited stateVisited; 
    }

    State current; // Estado actual

    // Displays de texto de la escena
    [SerializeField]
    public GameObject whoStarts; // Display de turno
    [SerializeField]
    public GameObject youDisplay; // Display de frase de jugador
    [SerializeField]
    public GameObject enemyDisplay; // Display de frase de enemigo

    // Prefabs que componen el marcador
    [SerializeField]
    public GameObject playerScoreboard; // Marcador de jugador
    [SerializeField]
    public GameObject enemyScoreboard; // Marcador de enemigo

    // Panel de botones
    [SerializeField]
    public Transform buttonPack; // Padre de los botones visibles
    [SerializeField]
    public Transform outsideContainer; // Padre de los botones ocultos
    [SerializeField]
    public GameObject prefabButton; // Botón prefab
    [SerializeField]
    public float firstPosY; // Altura donde colocar la primera instancia visible de botón
    [SerializeField]
    public GameObject arrowTop; // Botón que desplaza el panel de botones hacia arriba
    [SerializeField]
    public GameObject arrowBottom; // Botón que desplaza el panel de botones hacia abajo
    [SerializeField]
    public GameObject flipCoinButton; // Botón que lanza la moneda

    /// <summary>
    /// Display actual sobre el que escribir el insulto
    /// </summary>
    private GameObject currentDisplay;
    /// <summary>
    /// Texto actual a mostrar por display
    /// </summary>
    private string toDisplay;
    /// <summary>
    /// Indica que el jugadoir empezó la ronda
    /// </summary>
    private bool myTurn = false;
    /// <summary>
    /// Array de todos los pares de insultos existentes
    /// </summary>
    private InsultPair[] insults;
    /// <summary>
    /// Par de insultos (InsultPair) elegido por el jugador
    /// </summary>
    private InsultPair pairSelectedPlayer;
    /// <summary>
    /// Par de insultos (InsultPair) elegido por el enemigo
    /// </summary>
    private InsultPair pairSelectedEnemy;
    /// <summary>
    /// Array de instancias de los botones que contienen los insultos
    /// </summary>
    private GameObject[] insultButtons;
    /// <summary>
    /// Puntuación del jugador actual
    /// </summary>
    private int playerScore = 0;
    /// <summary>
    /// Puntuación del enemigo actual
    /// </summary>
    private int enemyScore = 0;
    /// <summary>
    ///  Rango de indices de botones visibles en el panel
    /// </summary>
    private int[] _visibleRange = { 0, 8 };

    /// <summary>
    ///  Rango de indices de botones visibles en el panel
    /// </summary>
    public int[] visibleRange {
        get { return _visibleRange; }
        set { _visibleRange = value; }
    }

    // Inicialización del juego
    void Start() {
        gameStarts();
    }

    /// <summary>
    /// Obtiene todos los pares de insultos y se guardan en el atributo insults
    /// </summary>
    void readFromJson() {
        insults = InsultPair.fromJson();
    }

    /// <summary>
    /// Inicializa los botones/flechas de desplazamiento por el panel
    /// </summary>
    void arrowSettings() {
        arrowTop.GetComponent<Button>().interactable = false;       // Desactivar botón
        arrowTop.GetComponent<ButtonEvents>().enabled = false;      // Desactivar efectos sonoros en botón
        arrowBottom.GetComponent<Button>().interactable = false;
        arrowBottom.GetComponent<ButtonEvents>().enabled = false;
    }

    /// <summary>
    /// Inicializa todo lo necesario antes de entrar en la máquina de estados
    /// </summary>
    void preSettings() {
        readFromJson();
        arrowSettings();
    }

    /// <summary>
    /// Crea la máquina de estados, obtiene el primer estado y prepara la moneda para empezar
    /// </summary>
    void gameStarts() {
        preSettings();              // Ajustes previos
        current = startMachine();   // Establece como estado actual el de inicio

        // Asigna un listener al botón de la moneda
        flipCoinButton.GetComponent<Button>().onClick.AddListener(() => {       // Cuando se le haga click
            flipCoinButton.GetComponent<Button>().interactable = false;         // Se desactiva el botón
            AudioAgent.coinFlipAudio.GetComponent<AudioSource>().Play();        // Reproduce el sonido de la moneda
            StartCoroutine(CoinAnimationController.flipIt(done => {             // Lanza la animación de la moneda
                current.stateVisited();                                         // Ejecuta las instrucciones del primer estado
            }));

        });
    }

    /// <summary>
    /// Obtiene un número aleatorio entre 0 y 1
    /// </summary>
    /// <returns>int</returns>
    int flipCoin() { 
        return Random.Range(0, 2);
    }

    /// <summary>
    /// Genera un insulto ataque aleatorio por parte de la máquina
    /// </summary>
    /// <param name="done">Callback booleano</param>
    /// <returns>IEnumerator</returns>
    IEnumerator randomIAFirst(System.Action<bool> done) {

        // Espera 1.5 segundos para dar la sensación de pensamiento en la máquina
        yield return new WaitForSeconds(1.5f); 

        pairSelectedEnemy = insults[Random.Range(0, insults.Length)];   // Elige uno insulto al azar de entre todos
        toDisplay = pairSelectedEnemy.first;
        done(true);
    }

    /// <summary>
    /// Genera un insulto espeusta aleatorio por parte de la máquina
    /// </summary>
    /// <param name="done">Callback booleano</param>
    /// <returns>IEnumerator</returns>
    IEnumerator randomIAReply(System.Action<bool> done) {

        // Comprueba si es una dificultad distinta a 'Easy'
        if((int)DifficultyAgent.GetDifficulty() != (int)DifficultyAgent.Difficulties.Easy) {

            // Crea un array de pares de insultos nuevo de tamaño reducido que depende de la dificultad
            InsultPair[] enemyPool = new InsultPair[(int)DifficultyAgent.GetDifficulty()]; 
            enemyPool[0] = pairSelectedPlayer; // Asigna a la posición 0 la respuesta correcta

            // Rellena el resto del array con respuestas aleatorias
            for (int i = 1; i < enemyPool.Length ; i++) {
                enemyPool[i] = insults[Random.Range(0, insults.Length)];
            }

            // Selecciona un par de insultos del nuevo 'pool'
            pairSelectedEnemy = enemyPool[Random.Range(0, enemyPool.Length)];
        } else
        {
            // Si está en fácil selecciona un par de insultos de entre todos los que existen
            pairSelectedEnemy = insults[Random.Range(0, insults.Length)];
        }      
        yield return new WaitForSeconds(1.5f);  // Espera 1.5 segundos 
        toDisplay = pairSelectedEnemy.reply;    // Establece el insulto a escribir en su Display
        done(true);                             // Lanza un callback para confirmar que ha terminado la ejecución
    }

    /// <summary>
    /// Muestra el texto actual en el display actual con un efecto de escritura en tiempo real
    /// </summary>
    /// <param name="done">Callback booleano</param>
    /// <returns>IEnumerator</returns>
    IEnumerator writeDynamicString(System.Action<bool> done) {

        // Cada 0.04 segundos va añadiendo un caracter al display
        for (int i = 0; i < toDisplay.Length; i++) {
            currentDisplay.GetComponent<Text>().text = toDisplay.Substring(0, i + 1);
            yield return new WaitForSeconds(0.04f);
        }
        done(true); // Comprueba que termine la ejecución
    }

    /// <summary>
    /// Actualiza el número de corazones que le queda a cada uno
    /// </summary>
    void drawScore() {

        // Obtiene el padre del marcador del jugador
        Transform transformPlayerScoreboard = playerScoreboard.GetComponent<Transform>(); 
        // Comprueba que (corazones de jugador + enemyScore == 3)
        if (transformPlayerScoreboard.childCount + enemyScore != 3) {
            // Elimina el siguiente corazón del jugador
            StartCoroutine(ScoreAnimationController.PopHearth(transformPlayerScoreboard.GetChild(0).gameObject));
        }

        // Obtiene el padre del marcador del enemigo
        Transform transformEnemyScoreboard = enemyScoreboard.GetComponent<Transform>();
        // Comprueba que (corazones de enemigo + playerScore == 3)
        if (transformEnemyScoreboard.childCount + playerScore != 3) {
            // Elimina el siguiente corazón del enemigo
            StartCoroutine(ScoreAnimationController.PopHearth(transformEnemyScoreboard.GetChild(0).gameObject));
        }
    }

    /// <summary>
    /// Actualiza las puntuaciones y determina si la partida ha llegado a su fin
    /// </summary>
    /// <returns>Termina la partida?</returns>
    bool updateScore() {

        bool maxScoreReached = false;                           // Por defecto no termina la partida

        if (playerScore > 2) {                                  
            PlayerAnimationController.Celebrate();              // Jugador hace animación de celebrar
            EnemyAnimationController.PretendsToBeShocked();     // Enemy hace animación de entrar en shock
            WinnerAgent.setwinner("PLAYER");
            maxScoreReached = true;                             // La partida está terminada
        }
        if (enemyScore > 2) {                                   
            PlayerAnimationController.PretendsToBeShocked();    // Jugador hace animación de entrar en shock
            EnemyAnimationController.Celebrate();               // Enemy hace animación de celebrar
            WinnerAgent.setwinner("ENEMY");
            maxScoreReached = true;                             // La partida esta terminada
        }

        drawScore(); // Actualiza los scoreboards

        // Devuelve si la partida a terminado o no (true = si, false = no)
        return maxScoreReached; 
    }

    /// <summary>
    ///  Comprueba quien gana, suma los puntos y establece el siguiente estado al que transitar
    /// </summary>
    /// <returns>Indice del siguiente estado</returns>
    int checkReply() {

        int nextStateIndex;

        // Si los pares de insultos seleccionados por ambos tienen la misma respuesta (Gana el que replica)
        if (pairSelectedPlayer.reply == pairSelectedEnemy.reply) {
            if (myTurn) {               // Si empezó la ronda el jugador
                enemyScore++;           
                nextStateIndex = 1;     // Se transita al estado current.nextState[1]
            } else {
                playerScore++;          
                nextStateIndex = 0;     // Se transita al estado current.nextState[0]
            }
        }
        else {                         
            if (myTurn) {
                playerScore++;
                nextStateIndex = 0;
            }
            else {
                enemyScore++;
                nextStateIndex = 1;
            }
        }
        
        // Comprueba si se ha terminado la partida
        if (updateScore())
                nextStateIndex = 2;     // Se transita al estado current.nextState[2]

        // Devuelve el índice del estado a transitar
        return nextStateIndex;
    }

    /// <summary>
    /// Establece el evento OnClick a cada botón
    /// </summary>
    /// <param name="button">Instancia de botón a asignarle el listener</param>
    /// <param name="index">Índice del botón</param>
    void setButtonListener(Button button, int index) {

        button.onClick.AddListener(() => {                              // Cuando se clicke el botón
            StartCoroutine(PlayerAnimationController.MoveToFight());    // El jugador se intenta mover a la zona de lucha
            pairSelectedPlayer = insults[index];                        // Asigna el par de insultos que contiene el botón según su índice
            if (myTurn)                                                 // Si empezó el jugador
                toDisplay = insults[index].first;                       // Muestra el ataque
            else
                toDisplay = insults[index].reply;                       // Muestra la réplica

            StartCoroutine(writeDynamicString(done => {
                current = current.nextState[0];                         // Transita al siguiente estado
                current.stateVisited();                                 // Ejecuta las instrucciones de la función delegada
            }));

            destroyButtons(); // Destruye los botones
        });
    }

    /// <summary>
    /// Activa todos los botones y sus efectos sonoros
    /// </summary>
    void enableButtons() {

        foreach (GameObject go in insultButtons) {
            go.GetComponent<Button>().interactable = true;
            go.GetComponent<ButtonEvents>().enabled = true;
        }
    }

    /// <summary>
    /// Desactiva todos los botones y sus efectos sonoros
    /// </summary>
    void disableButtons() {
        foreach (GameObject go in insultButtons) {
            go.GetComponent<Button>().interactable = false;
            go.GetComponent<ButtonEvents>().enabled = false;
        }
    }

    /// <summary>
    /// Destruye todas las instancias de botones
    /// </summary>
    void destroyButtons() {
        foreach (GameObject go in insultButtons)
        {
            Destroy(go);
        }
    }

    /// <summary>
    /// Limpia los displays de diálogo
    /// </summary>
    void clearDisplays() {
        youDisplay.GetComponent<Text>().text = "";
        enemyDisplay.GetComponent<Text>().text = "";
    }

    /// <summary>
    /// Modifica las posiciones de los botones dependiendo del rango visible y la dirección de desplazamiento
    /// </summary>
    /// <param name="increased">Aumenta el rango visible?</param>
    void moveButtons(bool increased) {

        if (increased) {        // Si aumenta el rango visible
            //El botón con índice, límite superior visible (visibleRange[1]), tiene que aparecer el último del padre de botones visible
            // Establece como padre de ese botón al padre de de botones visible
            insultButtons[visibleRange[1]].GetComponent<RectTransform>().SetParent(buttonPack);

            // Recoloca todos los botones en las posición de su anterior
            for(int i = visibleRange[1]; i >= visibleRange[0] ; i--) {
                insultButtons[i].GetComponent<RectTransform>().localPosition = insultButtons[i - 1].GetComponent<RectTransform>().localPosition;
            }

            // El botón con índice, anterior límite inferior (visibleRange[0] - 1]), desaparece del panel visible y pasa al padres de botones oculto
            insultButtons[visibleRange[0] - 1].GetComponent<RectTransform>().SetParent(outsideContainer);
            insultButtons[visibleRange[0] - 1].GetComponent<RectTransform>().localPosition = outsideContainer.localPosition;

        } else {                // Si disminuye ocurre lo mismo en sentido contrario
            insultButtons[visibleRange[0]].GetComponent<RectTransform>().SetParent(buttonPack);
            for (int i = visibleRange[0]; i <= visibleRange[1]; i++) {
                insultButtons[i].GetComponent<RectTransform>().localPosition = insultButtons[i + 1].GetComponent<RectTransform>().localPosition;
            }
            insultButtons[visibleRange[1] + 1].GetComponent<RectTransform>().SetParent(outsideContainer);
            insultButtons[visibleRange[1] + 1].GetComponent<RectTransform>().localPosition = outsideContainer.localPosition;
        }
    }

    /// <summary>
    /// Ajusta el estado de las flechas y de la navegación
    /// </summary>
    /// <param name="increased">Aumenta el rango visible?</param>
    void rangeChanged(bool increased) {
        if (increased) {
            if(visibleRange[1] == (insults.Length - 1)) {                   // Si el rango visible superior llega al límite
                arrowBottom.GetComponent<Button>().interactable = false;    // Desactiva el botón que desplaza el panel hacia abajo
                arrowBottom.GetComponent<ButtonEvents>().enabled = false;
            } else {                                                        // Si no, deberían estar todos activados
                if(!arrowBottom.GetComponent<Button>().interactable)
                    arrowBottom.GetComponent<Button>().interactable = true;
                if (!arrowTop.GetComponent<Button>().interactable)
                    arrowTop.GetComponent<Button>().interactable = true;
                if(!arrowBottom.GetComponent<ButtonEvents>().enabled)
                    arrowBottom.GetComponent<ButtonEvents>().enabled = true;
                if (!arrowTop.GetComponent<ButtonEvents>().enabled)
                    arrowTop.GetComponent<ButtonEvents>().enabled = true;
            }
        } else {
            if (visibleRange[0] == 0) {                                     // Si el rango visible inferior llega al límite
                arrowTop.GetComponent<Button>().interactable = false;       // Desactiva el botón que desplaza el panel hacia arriba
                arrowTop.GetComponent<ButtonEvents>().enabled = false;
            }
            else {                                                          // Si no, deberían estar todos activados
                if (!arrowBottom.GetComponent<Button>().interactable)
                    arrowBottom.GetComponent<Button>().interactable = true;
                if (!arrowTop.GetComponent<Button>().interactable)
                    arrowTop.GetComponent<Button>().interactable = true;
                if (!arrowTop.GetComponent<ButtonEvents>().enabled)
                    arrowTop.GetComponent<ButtonEvents>().enabled = true;
                if (!arrowBottom.GetComponent<ButtonEvents>().enabled)
                    arrowBottom.GetComponent<ButtonEvents>().enabled = true;
            }
        }
        
        //Actualiza la posicion de los botones
        moveButtons(increased);
    }

    /// <summary>
    /// Aumenta el rango visible
    /// </summary>
    public void arrowBottomClicked() {
        visibleRange[0]++;
        visibleRange[1]++;
        rangeChanged(true);  // Indica que ha habido un incremento del rango y reajusta la navegación
    }

    /// <summary>
    /// Disminuye el rango visible
    /// </summary>
    public void arrowTopClicked() {
        visibleRange[0]--;
        visibleRange[1]--;
        rangeChanged(false); // Indica que ha habido un decremento del rango y reajusta la navegación
    }

    /// <summary>
    /// Comprueba si se está en rango
    /// </summary>
    /// <param name="index">Número a buscar en el rango</param>
    /// <returns>Está en rango?</returns>
    bool inRange(int index) {

        if (index >= visibleRange[0] && index <= visibleRange[1])
            return true;
        else
            return false;
    }

    /// <summary>
    /// Instancia todos los botones
    /// </summary>
    void instantiateInsults() {
        int index = 0;
        float localY = firstPosY;
        Transform localTransform;

        // Las instancias de botones será igual al número de pares de insultos
        insultButtons = new GameObject[insults.Length];

        for (int i = 0; i < insults.Length; i++) {
            if (inRange(i))                     // Si el índice del par de insultos está en rango
                localTransform = buttonPack;    // Será hijo del padre de botones visibles
            else
                localTransform = outsideContainer; // Será hijo del padre de botones ocultos

            // Se instancia el botón
            GameObject go = Instantiate(prefabButton, localTransform);
            // Se almacena en el vector de instancias
            insultButtons[i] = go;

            if (inRange(i)) {                                                       // Si es un botón en rango visible
                Vector2 vsize = go.GetComponent<RectTransform>().sizeDelta;
                go.GetComponent<RectTransform>().localPosition = new Vector3(0, localY, 0); // Se establece la posición Y del botón
                localY = localY - vsize.y - 5f;                                     // Y = Y - Altura del botón - Espacio entre botones (5.0f)
            }

            if (myTurn) // Si empieza el jugador
                go.GetComponent<Button>().GetComponentInChildren<Text>().text = insults[i].first; // Los botones tendrán ataques
            else
            {
                go.GetComponent<Button>().GetComponentInChildren<Text>().text = insults[i].reply; // Los botones tendrán réplicas
                go.GetComponent<Button>().interactable = false;                                     // Estarán desactivados los botones y sus sonidos mientras se produce el ataque
                go.GetComponent<ButtonEvents>().enabled = false;
            }
            // Se establece el listener de cada instancia de botón
            setButtonListener(go.GetComponent<Button>(), index); 
            index++; 
        }
        
        if (insultButtons.Length > visibleRange[1] + 1) {                    // Si hay más botones que índice de rango superior
            arrowBottom.GetComponent<Button>().interactable = true;         // Activa la flecha que desplaza el panel de botones hacia abajo
            arrowBottom.GetComponent<ButtonEvents>().enabled = true;
        }

    }

    /// <summary>
    /// Carga la pantalla final
    /// </summary>
    /// <returns>IEnumerator</returns>
    IEnumerator gameOver() {
        yield return new WaitForSeconds(5f);
        MySceneManager.goToEnd();
    }

    /// <summary>
    /// Crea la máquina de estados
    /// </summary>
    /// <returns>Estado inicial</returns>
    public State startMachine() {
        State startS = createState(2);          // Estado inicial
        State playerAttackS = createState(1);   // Estado ataque de jugador
        State enemyAttackS = createState(1);    // Estado ataque de enemigo
        State playerReplyS = createState(1);    // Estado respuesta jugador
        State enemyReplyS = createState(1);     // Estado respuesta enemigo
        State checkReplyS = createState(3);     // Estado comprobar ronda
        State endS = createState(0);            // Estado final


        /* ESTADO INICIAL*/
        startS.nextState[0] = enemyAttackS;
        startS.nextState[1] = playerAttackS;
        startS.stateVisited = () => {
            current = current.nextState[flipCoin()]; // El siguiente estado será el que indique la moneda aleatoria
            current.stateVisited();                 
        };

        /* ESTADO ATAQUE DE ENEMIGO*/
        enemyAttackS.nextState[0] = playerReplyS;
        enemyAttackS.stateVisited = () => {
            whoStarts.GetComponent<Text>().text = "ENEMY STARTS";   // Se indica en el display que empieza el enemigo
            currentDisplay = enemyDisplay;                          // El display de texto a usar durante la estancia en este estado es el del enemigo
            myTurn = false;                                         // No empieza la ronda el jugador
            instantiateInsults();                                   // Se instancian los botones
            StartCoroutine(EnemyAnimationController.MoveToFight()); // El enemigo se mueve a la zona de lucha
            StartCoroutine(randomIAFirst(done => {                  // El enemigo elige un insulto ataque
                StartCoroutine(writeDynamicString(subdone => {      // Cuando esté elegido, lo escribe en el display
                    current = current.nextState[0];                 // Avanza al estado de réplica del jugador
                    current.stateVisited();
                }));
            })); 
        };

        /* ESTADO ATAQUE DE JUGADOR*/
        playerAttackS.nextState[0] = enemyReplyS;
        playerAttackS.stateVisited = () => {
            whoStarts.GetComponent<Text>().text = "YOU START"; // Se indica en el display que empieza el jugador
            currentDisplay = youDisplay;                     // El display de texto a usar durante la estancia en este estado es el del enemigo
            myTurn = true;                                   // Empieza la ronda el jugador
            instantiateInsults();                            
        };


        /* ESTADO RÉPLICA DE ENEMIGO*/
        enemyReplyS.nextState[0] = checkReplyS;
        enemyReplyS.stateVisited = () =>
        {
            currentDisplay = enemyDisplay;                          // El display a usar en este estado es el del enemigo
            StartCoroutine(EnemyAnimationController.MoveToFight());  // El enemigo se mueve a la zona de lucha
            StartCoroutine(randomIAReply(done => {                 // El enemigo elige un insulto réplica
                StartCoroutine(writeDynamicString(subdone => {      // Cuando esté elegido, lo escribe en el display
                    current = current.nextState[0];                // Avanza al estado de réplica comprobar ronda
                    current.stateVisited();                        
                })); 
            }));
        };

        /* ESTADO RÉPLICA DE JUGADOR*/
        playerReplyS.nextState[0] = checkReplyS;
        playerReplyS.stateVisited = () => {
            currentDisplay = youDisplay;    // El display a usar en este estado es el de jugador
            enableButtons();                // Se activan los botones  
        };


        /* ESTADO COMPROBAR RONDA*/
        checkReplyS.nextState[0] = playerAttackS;
        checkReplyS.nextState[1] = enemyAttackS;
        checkReplyS.nextState[2] = endS;
        checkReplyS.stateVisited = () =>
        {
            StartCoroutine(EnemyAnimationController.Fight());           // El enemigo se pone a pelear
            StartCoroutine(PlayerAnimationController.Fight((done) => {  // El jugador se pone a peleaR
                clearDisplays();                                        // Cuando terminan: Limpia los diálogos
                current = checkReplyS.nextState[checkReply()];          // Comprueba la ronda y determina la siguiente transición
                current.stateVisited();                                 
            }));
        };

        /* ESTADO FINAL*/
        endS.stateVisited = () => {
            StartCoroutine("gameOver");                 // Termina el juego
        };

        //Devuelve el estado inicial
        return startS;
    }

    /// <summary>
    /// Crea un estado
    /// </summary>
    /// <param name="transitions">Número de transiciones del estado</param>
    /// <returns>State</returns>
    private State createState(int transitions) {

        State state = new State();
        if(transitions > 0)
            state.nextState = new State[transitions];
        return state;
    }
}
