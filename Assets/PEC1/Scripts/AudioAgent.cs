﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Clase estática que centraliza todos los sonidos de la escena para
 * posteriormente poderlos reproducir sin necesitar agregar instacias
 * ni atributos adicionales al script que lleva la mayor carga del juego*/

public class AudioAgent : MonoBehaviour {

    // Objeto de escena que contiene el AudioSource del sonido que ocurre cuando se pasa por encima de un elemento el ratón
    [SerializeField]
    public GameObject mouseOver;

    // Objeto de escena que contiene el AudioSource del sonido que ocurre cuando se clicka un elemento
    [SerializeField]
    public GameObject clickIt;

    // Objeto de escena que contiene el AudioSource del sonido que ocurre cuando se pierde una vida
    [SerializeField]
    public GameObject popHearth;

    // Objeto de escena que contiene el AudioSource del sonido que ocurre cuando se clicka la moneda de sorteo de inicio de partida
    [SerializeField]
    public GameObject coinFlip;

    // Conjunto de objetos de escena que contienen AudioSource del sonido que ocurre cuando el personaje pega con el palo
    [SerializeField]
    public GameObject[] smashStick;

    // Atributos estáticos privados
    private static GameObject _mouseOverAudio;
    private static GameObject _clickItAudio;
    private static GameObject _popHearthAudio;
    private static GameObject _coinFlipAudio;
    private static GameObject[] _smashStickAudio;

    // Propiedades
    public static GameObject mouseOverAudio {
        get { return _mouseOverAudio; }
    }

    public static GameObject clickItAudio {
        get { return _clickItAudio; }
    }

    public static GameObject popHearthAudio {
        get { return _popHearthAudio; }
    }

    public static GameObject coinFlipAudio {
        get { return _coinFlipAudio; }
    }

    public static GameObject[] smashStickAudio {
        get { return _smashStickAudio; }
    }

    // Inicialización de los atributos privados cuando se inicia la escena
    void Start () {
        _mouseOverAudio = mouseOver;
        _clickItAudio = clickIt;
        _popHearthAudio = popHearth;
        _coinFlipAudio = coinFlip;
        _smashStickAudio = smashStick;
    }
}
