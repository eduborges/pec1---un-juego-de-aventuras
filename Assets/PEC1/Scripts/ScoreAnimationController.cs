﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

/* Clase que gestiona y produce los efectos de pérdida de vidas en los marcadores de juego */

public class ScoreAnimationController : MonoBehaviour {

    // Sprite de explosión
    [SerializeField]
    public Sprite popSprite;

    // Atributos privados
    private static Sprite _myPopSprite;

    // Propiedades
    public static Sprite myPopSprite {
        get { return _myPopSprite; }
        set { _myPopSprite = value; }
    }

    // Inicialización de los atributos
    void Start () {
        myPopSprite = popSprite;
    }

    /// <summary>
    /// Escala un GameObject a x=0.5, y=0.5, z=1.0
    /// </summary>
    /// <param name="thisOne">Objeto de escena a aplicar escala</param>
    static void ScaleDown(GameObject thisOne) {
        thisOne.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 1f);
    }

    /// <summary>
    /// Escala un GameObject a x=1.0, y=1.0, z=1.0
    /// </summary>
    /// <param name="thisOne">Objeto de escena a aplicar escala</param>
    static void ScaleUp(GameObject thisOne) {
        thisOne.GetComponent<RectTransform>().localScale = new Vector3(1f, 1f, 1f);
    }


    /// <summary>
    /// Crea un efecto de explosión y elimina el GameObject indicado
    /// </summary>
    /// <param name="thisOne">Objeto de escena a eliminar</param>
    /// <returns></returns>
    public static IEnumerator PopHearth(GameObject thisOne) {

        ScaleDown(thisOne);                                 // Reduce el sprite original
        yield return new WaitForSeconds(0.2f); 
        thisOne.GetComponent<Image>().sprite = myPopSprite; // Asigna como nuevo sprite al atributo de la clase
        ScaleUp(thisOne);                                   // Vuelve a su escala normal
        AudioAgent.popHearthAudio.GetComponent<AudioSource>().Play(); // Reproduce sonido de explosión de corazón
        yield return new WaitForSeconds(0.2f); 
        ScaleDown(thisOne); 
        yield return new WaitForSeconds(0.2f); 
        Destroy(thisOne);                                   // Destruye el objeto de la escena
    }

}
