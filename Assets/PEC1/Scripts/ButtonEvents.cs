﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;

/* Clase que maneja los eventos que se producen sobre los botones */
public class ButtonEvents : MonoBehaviour, IPointerEnterHandler, IPointerClickHandler
{
    /// <summary>
    /// Ejecuta el audio mouseOverAudio contenido en la clase AudioAgent cuando el ratón pasa por encima
    /// </summary>
    /// <param name="eventData">PointerEventData</param>
    public void OnPointerEnter(PointerEventData eventData) {
        AudioAgent.mouseOverAudio.GetComponent<AudioSource>().Play();
    }

    /// <summary>
    /// Ejecuta el audio clickItAudio contenido en la clase AudioAgent cuando el ratón clicka
    /// </summary>
    /// <param name="eventData">PointerEventData</param>
    public void OnPointerClick(PointerEventData eventData) {
        AudioAgent.clickItAudio.GetComponent<AudioSource>().Play();
    }
}
