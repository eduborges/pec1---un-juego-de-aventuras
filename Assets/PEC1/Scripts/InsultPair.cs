﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Clase cuyos valores pueden ser instanciados desde JSON
 * que contiene parejas de insultos (ataque, respuesta) asociados entre ellos */

[System.Serializable]
public class InsultPair {
    /// <summary>
    /// Insulto ataque
    /// </summary>
    public string _first;
    /// <summary>
    /// Insulto respuesta que gana al ataque (_first)
    /// </summary>
    public string _reply; 

    // Constructor
    public InsultPair(string firstInsult, string replyInsult) { 
        _first = firstInsult;
        _reply = replyInsult;
    }

    // Propiedades
    public string first {
        get { return _first; }
        set { _first = value; }
    }

    public string reply {
        get { return _reply; }
        set { _reply = value; }
    }

    /// <summary>
    /// Lee los insultos en forma de array desde un JSON
    /// </summary>
    /// <returns>Array de InsultPair</returns>
    public static InsultPair[] fromJson() {

        TextAsset jsonAsset = Resources.Load<TextAsset>("insults");

        // Se deserializa el JSON y se obtiene el objeto definido
        InsultPair[] jsonArray = JsonHelper.getJsonArray<InsultPair>(jsonAsset.ToString());

        return jsonArray;
    }
}

