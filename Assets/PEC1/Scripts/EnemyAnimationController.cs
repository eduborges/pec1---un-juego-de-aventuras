﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Clase que controla las transiciones del Animator del Enemigo */

public class EnemyAnimationController : MonoBehaviour {

    // Sprite en la escena del personaje 'Enemy'
    [SerializeField]
    public GameObject enemySprite;
    // Componente Animator del 'Enemy'
    [SerializeField]
    public Animator enemyAnimator;

    // Atributos estáticos privados
    private static GameObject _myEnemy;
    private static Animator _myAnimator;
    /// <summary>
    /// Bloquea la posibilidad de que el personaje se desplace más de una vez
    /// </summary>
    private static bool _blockMovement;


    // Propiedades del enemigo
    public static bool blockMovement {
        get { return _blockMovement; }
        set { _blockMovement = value; }
    }

    public static GameObject myEnemy {
        get { return _myEnemy; }
        set { _myEnemy  = value; }
    }

    public static Animator myAnimator {
        get { return _myAnimator; }
        set { _myAnimator = value; }
    }


    // Inicialización de los atributos
    void Start() {
        myEnemy = enemySprite;
        myAnimator = enemyAnimator;
        blockMovement = false;
    }

    /// <summary>
    /// Provoca el inicio de la animación de luchar (Enemy_fight)
    /// </summary>
    /// <returns>IEnumerator</returns>
    public static IEnumerator Fight() {
        // Transita al estado del animator de lucha
        myAnimator.SetBool("fight", true);

        // Espera 5 segundos en ese estado
        yield return new WaitForSeconds(5f);

        // Vuelve al estado de en guardia
        myAnimator.SetBool("fight", false);
    }


    /// <summary>
    /// Provoca la animación de celebrar (Enemy_victory)
    /// </summary>
    public static void Celebrate() {
        // Reescala al personaje para que no existan grandes diferencias de tamaño entre sprites
        myEnemy.GetComponent<RectTransform>().localScale = myEnemy.GetComponent<RectTransform>().localScale + new Vector3(-0.2f, -0.2f, 0f);

        // Transita hasta el estado con la animación de celebrar en el Animator
        myAnimator.SetBool("win", true);
    }

    /// <summary>
    /// Provoca la animación de entrar en shock (Enemy_defeat)
    /// </summary>
    public static void PretendsToBeShocked() {
        // Reescala al personaje para que no existan grandes diferencias de tamaño entre sprites
        myEnemy.GetComponent<RectTransform>().localScale = myEnemy.GetComponent<RectTransform>().localScale + new Vector3(-0.3f, -0.3f, 0f);

        // Transita hasta el estado con la animación de entrar en shock en el Animator
        myAnimator.SetBool("lose", true);
    }


    /// <summary>
    /// Mueve al 'Enemy' desde la posición de inicio hasta la posición de batalla y provoca las animaciones intermedias (Enemy_move, Enemy_engarde)
    /// </summary>
    /// <returns>IEnumerator</returns>
    public static IEnumerator MoveToFight() {
        // Comprueba si ya se ha desplazado anteriormente
        if (!blockMovement) {
            // Inicia la animación de movimiento (Enemy_move)
            myAnimator.SetBool("moving", true);

            // Número de pequeños desplazamientos que va a realizar para llegar a la posición destino
            for (int i = 0; i < 24; i++) {
                // En cada desplazamiento x = x - 5
                myEnemy.GetComponent<RectTransform>().localPosition = myEnemy.GetComponent<RectTransform>().localPosition + new Vector3(-5f, 0f, 0f);
                // Espera 0.04 segundos entre cada desplazamiento
                yield return new WaitForSeconds(0.04f);
            }

            // Al llegar al destino, detiene la animación de movimiento
            myAnimator.SetBool("moving", false);
            // Espera 0.08 segundos para hacer más real el posicionamiento
            yield return new WaitForSeconds(0.08f);
            // Inicia la animación de ponerse en guardia (Enemy_engarde)
            myAnimator.SetBool("engarde", true);
            // Activa el bloqueo de movimiento
            blockMovement = true;
        }
    }
}
