﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

/* Clase que centraliza el cambio de scenes */

public class MySceneManager : MonoBehaviour {

    /// <summary>
    /// Carga la scene 'MenuScene'
    /// </summary>
    public void goToMenu() {
        SceneManager.LoadScene("MenuScene");
    }

    /// <summary>
    /// Carga la scene 'GameScene'
    /// </summary>
    public void goToGame() {
        SceneManager.LoadScene("GameScene");
    }

    /// <summary>
    /// Carga la scene 'EndGame'
    /// </summary>
    public static void goToEnd() {
        SceneManager.LoadScene("EndGame");
    }
}
