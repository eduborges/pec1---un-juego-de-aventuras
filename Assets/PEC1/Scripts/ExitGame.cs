﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Clase que gestiona la salida del juego*/

public class ExitGame : MonoBehaviour {

    public void QuitGame(){
        Application.Quit();
    }
}
