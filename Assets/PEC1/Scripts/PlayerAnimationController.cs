﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Clase que controla las transiciones del Animator del Jugador */

public class PlayerAnimationController : MonoBehaviour {

    // Sprite en la escena del jugador
    [SerializeField]
    public GameObject playerSprite;
    // Componente Animator del jugador
    [SerializeField]
    public Animator playerAnimator;

    // Atributos estáticos privados
    private static GameObject _myPlayer;
    private static Animator _myAnimator;
    /// <summary>
    /// Bloquea la posibilidad de que el personaje se desplace más de una vez
    /// </summary>
    private static bool _blockMovement;


    // Propiedades del jugador
    public static bool blockMovement {
        get { return _blockMovement; }
        set { _blockMovement = value; }
    }

    public static GameObject myPlayer {
        get { return _myPlayer; }
        set { _myPlayer = value; }
    }

    public static Animator myAnimator {
        get { return _myAnimator; }
        set { _myAnimator = value; }
    }


    // Inicialización de los atributos
    void Start () {
        myPlayer = playerSprite;
        myAnimator = playerAnimator;
        blockMovement = false;
    }


    /// <summary>
    /// Provoca el inicio de la animación de luchar (Player_fight)
    /// </summary>
    /// <param name="done">Callback booleano</param>
    /// <returns>IEnumerator</returns>
    public static IEnumerator Fight(System.Action<bool> done) {
        // Transita al estado del animator de lucha
        myAnimator.SetBool("fight", true);

        yield return new WaitForSeconds(5f);

        // Vuelve al estado de en guardia
        myAnimator.SetBool("fight", false);
        
        done(true);
    }

    /// <summary>
    /// Provoca la animación de celebrar (Player_victory)
    /// </summary>
    public static void Celebrate() {

        // Reescala al personaje para que no existan grandes diferencias de tamaño entre sprites
        myPlayer.GetComponent<RectTransform>().localScale = myPlayer.GetComponent<RectTransform>().localScale + new Vector3(-0.2f, -0.2f, 0f);

        // Transita hasta el estado con la animación de celebrar
        myAnimator.SetBool("win", true);
    }

    /// <summary>
    /// Provoca la animación de entrar en shock (Player_defeat)
    /// </summary>
    public static void PretendsToBeShocked() {

        // Reescala al personaje para que no existan grandes diferencias de tamaño entre sprites
        myPlayer.GetComponent<RectTransform>().localScale = myPlayer.GetComponent<RectTransform>().localScale + new Vector3(-0.3f, -0.3f, 0f);

        // Transita hasta el estado con la animación de entrar en shock en el Animator
        myAnimator.SetBool("lose", true);
    }

    /// <summary>
    /// Mueve al jugador desde la posición de inicio hasta la posición de batalla y provoca las animaciones intermedias (Player_move, Player_engarde)
    /// </summary>
    /// <returns>IEnumerator</returns>
    public static IEnumerator MoveToFight() {
        // Comprueba si ya se ha desplazado anteriormente
        if (!blockMovement) {
            // Inicia la animación de movimiento (Enemy_move)
            myAnimator.SetBool("moving", true);

            // Número de pequeños desplazamientos que va a realizar para llegar a la posición destino
            for (int i = 0; i < 24; i++) {
                // En cada desplazamiento x = x + 5
                myPlayer.GetComponent<RectTransform>().localPosition = myPlayer.GetComponent<RectTransform>().localPosition + new Vector3(5f, 0f, 0f);
                // Espera 0.04 segundos entre cada desplazamiento
                yield return new WaitForSeconds(0.04f);
            }
            // Al llegar al destino, detiene la animación de movimiento
            myAnimator.SetBool("moving", false);
            // Espera 0.08 segundos para hacer más real el posicionamiento
            yield return new WaitForSeconds(0.08f);
            // Inicia la animación de ponerse en guardia (Player_engarde)
            myAnimator.SetBool("engarde", true);
            // Activa el bloqueo de moviemiento
            blockMovement = true;
        }
    }

    /// <summary>
    /// Reproduce un sonido de golpe aleatorio
    /// </summary>
    public void smashSound() { 
        AudioAgent.smashStickAudio[Random.Range(0, AudioAgent.smashStickAudio.Length)].GetComponent<AudioSource>().Play();
    }
}
