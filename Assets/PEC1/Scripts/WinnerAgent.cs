﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

/* Clase que almacena y muestra los datos del ganador de la partida */

public class WinnerAgent : MonoBehaviour {

    // Display de texto en el que puede escribir
    [SerializeField]
    public GameObject winnerDisplay;

    // Atributos privados
    /// <summary>
    /// Nombre del ganador
    /// </summary>
    private static string _winner; 

    //Propiedades
    public static string winner {
        get { return _winner; }
        set { _winner = value; }
    }

    /// <summary>
    /// Establece quien es el ganador
    /// </summary>
    /// <param name="winnerPlayer">Nombre del ganador</param>
    public static void setwinner(string winnerPlayer) {
        winner = winnerPlayer;
    }

    // Muestra el ganador
	void Start () {
        winnerDisplay.GetComponent<Text>().text = winner + " WINS";
	}
}
