STICK IN YOUR HEAD
==================

Este proyecto es la primera PEC de la asignatura **Programación Unity 2D** del **Master en diseño y desarrollo de videojuegos** de la **Universitat Oberta de Catalunya**.

Es una demo inspirada en el juego: [Monkey Island](https://www.youtube.com/watch?v=K91Lkz4XXFI)

Structure
---------

La estructura de datos principal que maneja el proyecto es un array de la clase InsultPair, cuyos atributos son 2 strings:

- first (el insulto ataque)
- reply (el insulto réplica que neutraliza a ese ataque)

Por lo que al manejar pares de insultos en un array, será fácil el acceso tanto por índice como por referencia a objeto.

Lo que guía las acciones del juego en cada momento es la máquina de estados definida internamente.

![StateMachine](/ReadmeMisc/SM_juego.png)


How to play
------------

- Flip the coin to know who starts.

![Coin](/Assets/PEC1/Images/SpriteSheets/01coin.png) 

- You are the green guy, enemy is the red.

![Player](/Assets/PEC1/Images/SpriteSheets/PlayerSpriteSheet.png) 
![Enemy](/Assets/PEC1/Images/SpriteSheets/EnemySpriteSheet.png) 

- Both have 3 lifes.
![Enemy](/Assets/PEC1/Images/hearthpink.png)

- The one who starts insults first.
- Then the other replies.
- If the reply is the one correct, who replies wins, else the first wins.


Play [here](https://alu0100885613.github.io/Stick-In-Your-Head/)

Gameplay
--------

[![U2D](http://img.youtube.com/vi/63MwI6vUlus/0.jpg)](http://www.youtube.com/watch?v=63MwI6vUlus "STICK IN YOUR HEAD")

Support
-------

mail: <eduborges@uoc.edu>

Author
------

[Eduardo Borges Fernández](https://gitlab.com/eduborges)

Credits
-------

- Sprites:

[Spinning pixel coin](https://opengameart.org/content/spinning-pixel-coin-0) by [irmirx](https://opengameart.org/users/irmirx) is licensed under [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/)

[Background](https://opengameart.org/content/background-6) by [jkjkke](https://opengameart.org/users/jkjkke) is licensed under [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/)


- Music:

[Loop - House In a Forest](https://opengameart.org/content/loop-house-in-a-forest) by [HorrorPen](https://opengameart.org/users/horrorpen) is licensed under [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/)

[forest](https://opengameart.org/content/forest) by [syncopika](https://opengameart.org/users/syncopika) is licensed under [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/)


- SoundEffects:

[Menu Selection Click](https://opengameart.org/content/menu-selection-click) by [NenadSimic](https://opengameart.org/users/nenadsimic) is licensed under [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/)

[Picked Coin Echo 2](https://opengameart.org/content/picked-coin-echo-2) by [NenadSimic](https://opengameart.org/users/nenadsimic) is licensed under [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/)


License
-------

The project is licensed under [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/).